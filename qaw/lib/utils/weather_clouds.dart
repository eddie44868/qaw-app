import 'package:flutter/material.dart';

WeatherClouds(String cuaca, double number) {
  if (cuaca == 'Cerah Berawan') {
    return Image.asset(
      'assets/images/qaw (5).png',
      height: number,
    );
  } if (cuaca == 'Berawan') {
    return Image.asset(
      'assets/images/qaw_awan.png',
      height: number,
    );
  } if (cuaca == 'Kabut' || cuaca == 'Berkabut Tebal') {
    return Image.asset(
      'assets/images/cloud (1).png',//
      height: number,
    );
  } else {
    return Image.asset(
      'assets/images/sun.png',
      height: number,
    );
  } 
}