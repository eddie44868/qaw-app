import 'package:flutter/material.dart';

import 'package:qaw/utils/weather_clouds.dart';
import 'package:qaw/utils/weather_home.dart';

// ignore: must_be_immutable
class WeatherContainer extends StatelessWidget {
  String name;
  String humi;
  String mph;
  String value;
  String wd;
  String time;
  WeatherContainer({
    Key? key,
    required this.name,
    required this.humi,
    required this.mph,
    required this.value,
    required this.wd,
    required this.time,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10),
      child: Center(
        child: Column(
          children: [
            WeatherClouds(name, 150),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      'Waktu',
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      time,
                      style: TextStyle(
                          fontSize: 40,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  width: 30,
                ),
                Column(
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      humi, //
                      style: TextStyle(
                          fontSize: 40,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                )
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    WeatherHome(
                        title: mph,
                        img: 'qaw (10)',
                        desc: 'Kecepatan Angin'),
                    WeatherHome(
                        title: value, img: 'qaw (8)', desc: 'Kelembapan'),
                  ],
                ),
                SizedBox(
                  width: 9,
                ),
                Image.asset(
                  'assets/images/Line 1.png',
                  height: 130,
                ),
                SizedBox(
                  width: 9,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    WeatherHome(
                        title: humi, img: 'qaw (9)', desc: 'Temperatur'),
                    WeatherHome(title: wd, img: 'qaw (11)', desc: 'Arah Angin'),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
