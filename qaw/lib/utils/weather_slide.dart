import 'package:flutter/material.dart';
import 'package:qaw/utils/weather_clouds.dart';

class WeatherSlide extends StatelessWidget {
  final String title;
  final String img;
  final String desc;
  const WeatherSlide({super.key, required this.title, required this.img, required this.desc});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 100,
              width: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xff92BADF),
                boxShadow: [
                  BoxShadow(
                    color: const Color.fromARGB(255, 51, 51, 51).withOpacity(0.7),
                    blurRadius: 1,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    WeatherClouds(img, 30),
                    Text(
                      desc,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(width:15,),
          ],
        ),
        SizedBox(height:15,),
      ],
    );
  }
}
