import 'package:flutter/material.dart';

class QuakeData extends StatelessWidget {
  final String img;
  final String text;
  final String desc;
  const QuakeData({super.key, required this.img, required this.text, required this.desc});

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(color: Colors.white),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      // columnWidths: {
      //   0: FlexColumnWidth(4),
      //   1: FlexColumnWidth(5),
      // },
      children: [
        TableRow(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
              children: [
                Image.asset(
                  'assets/images/$img.png',
                  height: 30,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  text,
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.white),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              child: Text(
                desc,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: Colors.white),
              ),
            ),
          ]
        )
      ],
    );
  }
}
