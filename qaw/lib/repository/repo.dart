import 'package:http/http.dart';
import 'dart:convert';
import '../models/weather_model.dart';

class QawRepository {
  Future<List<Param>> getWeather(String city, String prov) async {
    Response response = await get(Uri.parse("https://cuaca-gempa-rest-api.vercel.app/weather/${prov}/${city}"));
    if (response.statusCode == 200) {
      final List result = jsonDecode(response.body)['data']['params'];
      return result.map(((e) => Param.fromJson(e))).toList();
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
  Future getQuake() async {
    Response r = await get(Uri.parse("https://cuaca-gempa-rest-api.vercel.app/quake"));
    if (r.statusCode == 200) {
      Map h = jsonDecode(r.body)['data'];////
      final List result = h.values.toList();
      print(result);
      return result;//
    } else {
      throw Exception(r.reasonPhrase);
    }
  }
}