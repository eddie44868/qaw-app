class Param {
    Param({
        required this.id,
        required this.description,
        required this.type,
        required this.times,
    });

    final String? id;
    final String? description;
    final String? type;
    final List<Time> times;

    factory Param.fromJson(Map<String, dynamic> json){ 
        return Param(
            id: json["id"],
            description: json["description"],
            type: json["type"],
            times: json["times"] == null ? [] : List<Time>.from(json["times"]!.map((x) => Time.fromJson(x))),
        );
    }

}

class Time {
    Time({
        required this.type,
        required this.h,
        required this.datetime,
        required this.value,
        required this.day,
        required this.celcius,
        required this.fahrenheit,
        required this.code,
        required this.name,
        required this.deg,
        required this.card,
        required this.sexa,
        required this.kt,
        required this.mph,
        required this.kph,
        required this.ms,
    });

    final String? type;
    final String? h;
    final String? datetime;
    final String? value;
    final String? day;
    final String? celcius;
    final String? fahrenheit;
    final String? code;
    final String? name;
    final String? deg;
    final String? card;
    final String? sexa;
    final String? kt;
    final String? mph;
    final String? kph;
    final String? ms;

    factory Time.fromJson(Map<String, dynamic> json){ 
        return Time(
            type: json["type"],
            h: json["h"],
            datetime: json["datetime"],
            value: json["value"],
            day: json["day"],
            celcius: json["celcius"],
            fahrenheit: json["fahrenheit"],
            code: json["code"],
            name: json["name"],
            deg: json["deg"],
            card: json["card"],
            sexa: json["sexa"],
            kt: json["kt"],
            mph: json["mph"],
            kph: json["kph"],
            ms: json["ms"],
        );
    }

}
