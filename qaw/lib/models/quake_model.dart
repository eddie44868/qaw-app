class QuakeModel {
    QuakeModel({
        required this.success,
        required this.message,
        required this.data,
    });

    final bool? success;
    final dynamic message;
    final Keterangan? data;

    factory QuakeModel.fromJson(Map<String, dynamic> json){ 
        return QuakeModel(
            success: json["success"],
            message: json["message"],
            data: json["data"] == null ? null : Keterangan.fromJson(json["data"]),
        );
    }

}

class Keterangan {
    Keterangan({
        required this.tanggal,
        required this.jam,
        required this.datetime,
        required this.coordinates,
        required this.lintang,
        required this.bujur,
        required this.magnitude,
        required this.kedalaman,
        required this.wilayah,
        required this.potensi,
        required this.dirasakan,
        required this.shakemap,
    });

    final String? tanggal;
    final String? jam;
    final DateTime? datetime;
    final String? coordinates;
    final String? lintang;
    final String? bujur;
    final String? magnitude;
    final String? kedalaman;
    final String? wilayah;
    final String? potensi;
    final String? dirasakan;
    final String? shakemap;

    factory Keterangan.fromJson(Map<String, dynamic> json){ 
        return Keterangan(
            tanggal: json["tanggal"],
            jam: json["jam"],
            datetime: DateTime.tryParse(json["datetime"] ?? ""),
            coordinates: json["coordinates"],
            lintang: json["lintang"],
            bujur: json["bujur"],
            magnitude: json["magnitude"],
            kedalaman: json["kedalaman"],
            wilayah: json["wilayah"],
            potensi: json["potensi"],
            dirasakan: json["dirasakan"],
            shakemap: json["shakemap"],
        );
    }

}
