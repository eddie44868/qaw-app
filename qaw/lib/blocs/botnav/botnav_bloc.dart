import 'package:bloc/bloc.dart';

part 'botnav_event.dart';
part 'botnav_state.dart';

class BotnavBloc extends Bloc<BotnavEvent, BotnavState> {
  BotnavBloc() : super(BotnavLandingInitial(tabIndex: 0)) {
    on<BotnavEvent>((event, emit) {
      if (event is TabChange) {
        emit(BotnavLandingInitial(tabIndex: event.tabIndex));
      }
    });
  }
}
