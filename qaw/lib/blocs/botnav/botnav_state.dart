part of 'botnav_bloc.dart';

sealed class BotnavState {
  final int tabIndex;
  const BotnavState({required this.tabIndex});
  
}

class BotnavLandingInitial extends BotnavState {
  BotnavLandingInitial({required super.tabIndex});
}
