part of 'botnav_bloc.dart';

sealed class BotnavEvent {}

class TabChange extends BotnavEvent {
  final int tabIndex;

  TabChange({required this.tabIndex});
}
