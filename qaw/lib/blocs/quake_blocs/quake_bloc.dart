import 'package:bloc/bloc.dart';
import 'package:qaw/repository/repo.dart';

part 'quake_event.dart';
part 'quake_state.dart';

class QuakeBloc extends Bloc<QuakeEvent, QuakeState> {
  QawRepository _quakeRepository;

  QuakeBloc(this._quakeRepository) : super(QuakeLoadingState()) {
    on<LoadedQuakeEvent>((event, emit) async {
      emit(QuakeLoadingState());
      try {
          final List quake = await _quakeRepository.getQuake();
          emit(QuakeLoadedState(dataList: quake));
      } catch (e) {
        emit(QuakeErrorState(errorMsg: e.toString()));
      }
    });
  }
}
