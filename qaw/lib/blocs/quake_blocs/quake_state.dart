part of 'quake_bloc.dart';

sealed class QuakeState {}

class QuakeLoadingState extends QuakeState {}

class QuakeLoadedState extends QuakeState {
  final List dataList;
  QuakeLoadedState({
    required this.dataList,
  });
}

class QuakeErrorState extends QuakeState {
  final String errorMsg;
  QuakeErrorState({
    required this.errorMsg,
  });
}
