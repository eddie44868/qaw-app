part of 'quake_bloc.dart';

sealed class QuakeEvent {}

final class LoadedQuakeEvent extends QuakeEvent {}
