part of 'qaw_bloc.dart';

sealed class QawState {}

class QawLoadingState extends QawState {}

class QawLoadedState extends QawState {
  final List<Param> dataList;
  QawLoadedState({
    required this.dataList,
  });
}

class QawErrorState extends QawState {
  final String errorMsg;
  QawErrorState({
    required this.errorMsg,
  });
}
