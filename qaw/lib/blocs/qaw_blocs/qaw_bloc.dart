import 'package:bloc/bloc.dart';
import 'package:qaw/models/weather_model.dart';
import 'package:qaw/repository/repo.dart';

part 'qaw_event.dart';
part 'qaw_state.dart';

class QawBloc extends Bloc<QawEvent, QawState> {
  QawRepository _qawRepository;

  QawBloc(this._qawRepository,) : super(QawLoadingState()) {
    on<LoadedWeatherEvent>((event, emit) async {
      emit(QawLoadingState());
      try {
        final List<Param> qaw = await _qawRepository.getWeather(event.kota, event.prov);
        emit(QawLoadedState(dataList: qaw));
      } catch (e) {
        emit(QawErrorState(errorMsg: e.toString()));
      }
    });
  }
}
    
