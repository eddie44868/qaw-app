part of 'qaw_bloc.dart';

sealed class QawEvent {}

class LoadedWeatherEvent extends QawEvent {
  String kota;
  String prov;
  LoadedWeatherEvent({
    required this.kota,
    required this.prov,
  });

}//
