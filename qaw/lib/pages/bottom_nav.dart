import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../blocs/botnav/botnav_bloc.dart';
import '../repository/repo.dart';
import 'home_screen.dart';
import 'quake_screen.dart';

// ignore: must_be_immutable
class BottomNav extends StatefulWidget {
  String city;
  String prov;
  String add;
  BottomNav({
    Key? key,
    required this.city,
    required this.prov,
    required this.add,
  }) : super(key: key);

  @override
  State<BottomNav> createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  var count = 0;
 
  exitPush() {//
    count++;
    if (count == 2) {
      exit(0);
    } else {
      Fluttertoast.showToast( 
      msg: "Tekan 2 kali untuk keluar",
      );
      return false;
    }
  }
  @override
  Widget build(BuildContext context) {
    List<Widget> _pages = <Widget>[
      RepositoryProvider<QawRepository>(
        create: (context) => QawRepository(),
        child: HomeScreen(city: widget.city, prov: widget.prov, add: widget.add,),
      ),
      RepositoryProvider<QawRepository>(
        create: (context) => QawRepository(),
        child: QuakeScreen(),
      )
    ];

    return WillPopScope(
      onWillPop: () {
        return exitPush();
      },
      child: BlocBuilder<BotnavBloc, BotnavState>(
        builder: (context, state) {
          return Scaffold(
              body: _pages.elementAt(state.tabIndex),
              bottomNavigationBar: BottomAppBar(
                color: const Color(0xFF92BADF),
                child: Container(
                  height: 60,
                  color: const Color(0xFF9398D6),
                  padding: EdgeInsets.symmetric(vertical: 3, horizontal: 3),
                  child: Row(
                    children: [
                      BottomNavMenu(
                        label: "Cuaca",
                        icon: 'assets/images/cloudy-day.png',
                        isSelected: state.tabIndex.isEven,
                        onTap: () {
                          BlocProvider.of<BotnavBloc>(context)
                              .add(TabChange(tabIndex: 0));
                        },
                      ),
                      VerticalDivider(),
                      BottomNavMenu(
                        label: "Gempa",
                        icon: 'assets/images/earthquake (4).png',
                        isSelected: state.tabIndex.isOdd,
                        onTap: () {
                          BlocProvider.of<BotnavBloc>(context)
                              .add(TabChange(tabIndex: 1));
                        },
                      ),
                    ],
                  ),
                ),
              ));
        },
      ),
    );
  }
}

class BottomNavMenu extends StatelessWidget {
  final Function()? onTap;
  final String label;
  final String icon;
  final bool isSelected;
  const BottomNavMenu({
    Key? key,
    this.onTap,
    required this.label,
    required this.icon,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Image.asset(
            icon,
            height: 25,
            color: isSelected ? Colors.white : Colors.grey[350],
          ),
          Text(
            label,
            style: TextStyle(
              fontSize: 15,
              color: isSelected ? Colors.white : Colors.grey[350],
            ),
          )
        ],
      ),
    ));
  }
}
