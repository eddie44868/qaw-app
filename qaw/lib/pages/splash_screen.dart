import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:qaw/blocs/botnav/botnav_bloc.dart';
import 'package:qaw/pages/bottom_nav.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String currentAddress = '';
  String city = '';
  String prov = '';

  Future<void> getLocation() async {//
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);
    Placemark place = placemarks[0];
    String street ="${place.subAdministrativeArea}, ${place.administrativeArea}";
    String kota = place.subAdministrativeArea!.split(' ').last;
    String propinsi = place.administrativeArea!.replaceAll(' ', '-').toLowerCase();
      setState(() {
        city = kota;
        prov = propinsi;
        currentAddress = street;
      });
  }

  @override
  void initState() {
    super.initState();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    final double heightScreen = MediaQuery.of(context).size.height;
    Future.delayed(const Duration(seconds: 3), () {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => BlocProvider(
                create: (context) => BotnavBloc(),
                child: BottomNav(city: city, prov: prov, add: currentAddress,),
              )));
    });
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: heightScreen / 2,
                width: widthScreen,
                decoration: BoxDecoration(
                  color: const Color(0xFF9398D6),
                ),
              ),
              Container(
                height: heightScreen / 2,
                width: widthScreen,
                decoration: BoxDecoration(
                  color: const Color(0xFF92BADF),
                ),
              ),
            ],
          ),
          Center(
              child: Image.asset(
            'assets/images/cloud.png',
            width: widthScreen,
            color: Colors.white.withOpacity(0.3),
            colorBlendMode: BlendMode.modulate,
          )),
          Center(
              child: Container(
            height: 120,
            width: 120,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: const Color.fromARGB(255, 51, 51, 51).withOpacity(0.7),
                  blurRadius: 1,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: Image.asset(
              'assets/images/qaw_2.png',
              width: 120,
              height: 120,
            ),
          )),
        ],
      ),
    );
  }
}
