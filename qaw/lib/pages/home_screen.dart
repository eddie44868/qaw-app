import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:qaw/models/weather_model.dart';
import 'package:qaw/repository/repo.dart';
import 'package:qaw/utils/Weather_container.dart';
import '../blocs/qaw_blocs/qaw_bloc.dart';
import '../utils/weather_slide.dart';

// ignore: must_be_immutable
class HomeScreen extends StatefulWidget {
  String city;
  String prov;
  String add;
  HomeScreen({
    Key? key,
    required this.city,
    required this.prov,
    required this.add,
  }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DateTime now = DateTime.now();
  late int formattedTime = int.parse(DateFormat('kk').format(now));
  late String timeNow = DateFormat('kk:mm').format(now);

  @override
  Widget build(BuildContext context) {
    final List j = [];
    final double widthScreen = MediaQuery.of(context).size.width;
    final double heightScreen = MediaQuery.of(context).size.height;

    return BlocProvider<QawBloc>(
      create: (context) => QawBloc(
        RepositoryProvider.of<QawRepository>(context),
      )..add(LoadedWeatherEvent(kota: widget.city, prov: widget.prov)),
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Image.asset(
              'assets/images/qaw (14).png',
              height: 50,
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.grey[350],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.home,
                            size: 20,
                            color: Colors.grey[700],
                          ),
                          (widget.add == '')
                              ? Row(
                                  children: [
                                    Icon(Icons.error_outline,
                                        size: 20, color: Colors.grey[700]),
                                    SizedBox(width: 10),
                                    Text('Lokasi tidak ditemukan!')
                                  ],
                                )
                              : Text(widget.add),
                          SizedBox(width: 10),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    height: heightScreen / 1.8,
                    width: widthScreen,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: const Color(0xFF92BADF),
                      boxShadow: [
                        BoxShadow(
                          color: const Color.fromARGB(255, 51, 51, 51).withOpacity(0.7),
                          blurRadius: 1,
                          offset: Offset(0, 2),
                        ),
                      ],
                    ),
                    child: BlocBuilder<QawBloc, QawState>(
                      builder: (context, state) {
                        if (state is QawLoadingState) {
                          print("qaw loading");
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else if (state is QawLoadedState) {
                          List<Param> qawList = state.dataList;
                          print("qaw loaded");
                          var date1 = qawList[5].times[0].datetime.toString();
                          var time1 = date1.substring(8, 12);
                          var jam1 = int.parse(time1.substring(0, 2));

                          var date2 = qawList[5].times[1].datetime.toString();
                          var time2 = date2.substring(8, 12);
                          var jam2 = int.parse(time2.substring(0, 2));

                          var date3 = qawList[5].times[2].datetime.toString();
                          var time3 = date3.substring(8, 12);
                          var jam3 = int.parse(time3.substring(0, 2));

                          var date4 = qawList[5].times[3].datetime.toString();
                          var time4 = date4.substring(8, 12);
                          var jam4 = int.parse(time4.substring(0, 2));

                          if (formattedTime >= jam1 && formattedTime <= jam2) {
                            var listed = qawList[5].times[0].celcius.toString();
                            var hu = listed.substring(0, 2) +
                                " °" +
                                listed.substring(3, 4);

                            var angin = qawList[7].times[0].card.toString();
                            var wd = angin.substring(0, 3);

                            var kec = qawList[8].times[0].mph.toString();
                            var mph = kec.substring(0, 4) + ' mph';

                            return WeatherContainer(
                                name: qawList[6].times[0].name.toString(),
                                humi: hu,
                                mph: mph,
                                value: qawList[0].times[0].value.toString(),
                                wd: wd,
                                time: timeNow,);
                          } else if (formattedTime >= jam2 &&
                              formattedTime <= jam3) {
                            var listed = qawList[5].times[1].celcius.toString();
                            var hu = listed.substring(0, 2) +
                                " °" +
                                listed.substring(3, 4);

                            var angin = qawList[7].times[1].card.toString();
                            var wd = angin.substring(0, 3);

                            var kec = qawList[8].times[1].mph.toString();
                            var mph = kec.substring(0, 4) + ' mph';

                            return WeatherContainer(
                                name: qawList[6].times[1].name.toString(),
                                humi: hu,
                                mph: mph,
                                value: qawList[0].times[1].value.toString(),
                                wd: wd,
                                time: timeNow,);
                          } else if (formattedTime >= jam3 &&
                              formattedTime <= jam4) {
                            var listed = qawList[5].times[2].celcius.toString();
                            var hu = listed.substring(0, 2) +
                                " °" +
                                listed.substring(3, 4);

                            var angin = qawList[7].times[2].card.toString();
                            var wd = angin.substring(0, 3);

                            var kec = qawList[8].times[2].mph.toString();
                            var mph = kec.substring(0, 4) + ' mph';

                            return WeatherContainer(
                                name: qawList[6].times[2].name.toString(),
                                humi: hu,
                                mph: mph,
                                value: qawList[0].times[2].value.toString(),
                                wd: wd,
                                time: timeNow,);
                          } else {
                            var listed = qawList[5].times[3].celcius.toString();
                            var hu = listed.substring(0, 2) +
                                " °" +
                                listed.substring(3, 4);

                            var angin = qawList[7].times[3].card.toString();
                            var wd = angin.substring(0, 3);

                            var kec = qawList[8].times[3].mph.toString();
                            var mph = kec.substring(0, 4) + ' mph';

                            return WeatherContainer(
                                name: qawList[6].times[3].name.toString(),
                                humi: hu,
                                mph: mph,
                                value: qawList[0].times[3].value.toString(),
                                wd: wd,
                                time: timeNow,);
                          }
                        }
                        if (state is QawErrorState) {
                          print("Error");
                          return Container(
                            height: heightScreen / 1.8,
                            width: widthScreen,
                            child: Center(
                                child: Icon(
                              Icons.error_outline,
                              size: 150,
                              color: Colors.grey[700],
                            )),
                          );
                        }
                        return Container();
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Besok',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  BlocBuilder<QawBloc, QawState>(
                    builder: (context, state) {
                      if (state is QawLoadingState) {
                        print("qaw loading");
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is QawLoadedState) {
                        List<Param> qawListed = state.dataList;
                        List c = [];
                        for (var i = 4; i < 8; i++) {
                          var besok = qawListed[5].times[i].celcius.toString();
                          var tom = besok.substring(0, 2) +
                              " °" +
                              besok.substring(3, 4);
                          c.add(tom);
                        }
                        for (var i = 4; i < 8; i++) {
                          var jam = qawListed[5].times[i].datetime.toString();
                          var time = jam.substring(8, 10) +
                              ":" +
                              jam.substring(10, 12);
                          j.add(time);
                        }
                        return SizedBox(
                            height: 115,
                            width: widthScreen,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: 4,
                                itemBuilder: (context, index) {
                                  return WeatherSlide(
                                      title: j[index],
                                      img: qawListed[6]
                                          .times[index + 3]
                                          .name
                                          .toString(),
                                      desc: c[index]);
                                }));
                      }
                      if (state is QawErrorState) {
                        print("Error");
                        return SizedBox(
                          height: 115,
                          width: widthScreen,
                          child: Center(
                              child: Icon(
                            Icons.error_outline,
                            size: 100,
                            color: Colors.grey[700],
                          )),
                        );
                      }
                      return Container();
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Lusa',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  BlocBuilder<QawBloc, QawState>(
                    builder: (context, state) {
                      if (state is QawLoadingState) {
                        print("qaw loading");
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else if (state is QawLoadedState) {
                        List<Param> qawListed = state.dataList;
                        List c = [];
                        for (var i = 8; i < 12; i++) {
                          var besok = qawListed[5].times[i].celcius.toString();
                          var tom = besok.substring(0, 2) +
                              " °" +
                              besok.substring(3, 4);
                          c.add(tom);
                        }
                        for (var i = 8; i < 12; i++) {
                          var jam = qawListed[5].times[i].datetime.toString();
                          var time = jam.substring(8, 10) +
                              ":" +
                              jam.substring(10, 12);
                          j.add(time);
                        }

                        return SizedBox(
                            height: 115,
                            width: widthScreen,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: 4,
                                itemBuilder: (context, index) {
                                  return WeatherSlide(
                                      title: j[index],
                                      img: qawListed[6]
                                          .times[index + 8]
                                          .name
                                          .toString(),
                                      desc: c[index]);
                                }));
                      }
                      if (state is QawErrorState) {
                        print("Error");
                        return SizedBox(
                          height: 115,
                          width: widthScreen,
                          child: Center(
                              child: Icon(
                            Icons.error_outline,
                            size: 100,
                            color: Colors.grey[700],
                          )),
                        );
                      }
                      return Container();
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
