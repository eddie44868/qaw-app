import 'package:flutter/material.dart';
import 'package:qaw/utils/quake_data.dart';
import '../blocs/quake_blocs/quake_bloc.dart';
import '../repository/repo.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class QuakeScreen extends StatefulWidget {
  const QuakeScreen({super.key});

  @override
  State<QuakeScreen> createState() => _QuakeScreenState();
}

class _QuakeScreenState extends State<QuakeScreen> {
  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    final double heightScreen = MediaQuery.of(context).size.height;

    return BlocProvider(
      create: (context) =>
          QuakeBloc(RepositoryProvider.of<QawRepository>(context))
            ..add(LoadedQuakeEvent()),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xff92BADF),
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Image.asset(
              'assets/images/qaw (14).png',
              height: 50,
            ),
            centerTitle: true,
            backgroundColor: Color(0xff92BADF),
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Center(
                child: BlocBuilder<QuakeBloc, QuakeState>(
                  builder: (context, state) {
                    if (state is QuakeLoadingState) {
                      print("quake loading");

                      return Container(
                        height: heightScreen-150,
                        width: widthScreen-20,
                        child: Center(
                          child: CircularProgressIndicator()),
                      );
                    } else if (state is QuakeLoadedState) {
                      List quake = state.dataList;
                      print("quake loaded");
                      
                      return Container(
                        width: widthScreen - 20,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: const Color(0xFF9398D6)),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Gempa Bumi',
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: Image.network(
                                quake[11],
                                height: 300,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: widthScreen - 50,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color(0xff92BADF),
                              ),
                              child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 10),
                                  child: Column(
                                    children: [
                                      QuakeData(
                                          img: 'image (14)',
                                          text: 'Tgl/Waktu',
                                          desc: '${quake[0]}  ${quake[1]}'),
                                      QuakeData(
                                          img: 'image (8)',
                                          text: 'Koordinat',
                                          desc: quake[3]),
                                      QuakeData(
                                          img: 'image (9)',
                                          text: 'Magnitude',
                                          desc: '${quake[6]} SR'),
                                      QuakeData(
                                          img: 'image (10)',
                                          text: 'Kedalaman',
                                          desc: quake[7]),
                                      QuakeData(
                                          img: 'image (11)',
                                          text: 'Wilayah',
                                          desc: quake[8]),
                                      QuakeData(
                                          img: 'image (12)',
                                          text: 'Potensi',
                                          desc: quake[9]),
                                      QuakeData(
                                          img: 'image (13)',
                                          text: 'Dirasakan',
                                          desc: quake[10]),
                                    ],
                                  )),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      );
                    }
                    if (state is QuakeErrorState) {
                      print("Error");
                      return Center(
                        child: Text("Quake Error"),
                      );
                    }
                    return Container();
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}